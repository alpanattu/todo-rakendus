<?php
interface TodoItemDao {
   public function save($todoItem);
   public function update($todoItem);
   public function findAll();
   public function findById($id);
   public function deleteAll();
   public function deleteById($id);
}