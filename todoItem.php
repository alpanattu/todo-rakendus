<?php
class TodoItem {
    public $id;
    public $name;
    public $dateAdded;
    public $comments = [];

    public static function fromAssocArray($array) {
        $date = isset($array["dateAdded"]) ? $array["dateAdded"] : date("Y-m-d");
        $id = isset($array["id"]) ? $array["id"] : null;
        $todoItem = new TodoItem($array["name"], $date, $id);
        if (isset($array["comments"])) {
            $todoItem->comments = $array["comments"];
        }

        return $todoItem;
    }

    function __construct($name, $dateAdded, $id = null) {
        $this->name = $name;
        $this->dateAdded = $dateAdded;
        $this->id = $id;
    }

    function add_comment($comment) {
        if (isset($comment)) {
            $this->comments[] = $comment;
        }
    }

    public function validate() {
        $errorMessages = [];

        if (empty($this->name)) {
            $errorMessages[] = "Todo item ei või olla tühi!";
        }

        if (strlen($this->name) < 3) {
            $errorMessages[] = "Pikkus peab olema vähemalt 3 tähemärki";
        }

        if (strlen($this->name) > 20) {
            $errorMessages[] = "Pikku ei tohi olla suurem kui 20 tähemärki";
        }

        return $errorMessages;
    }
}